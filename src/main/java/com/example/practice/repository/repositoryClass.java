package com.example.practice.repository;

import com.example.practice.entity.entityClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface repositoryClass extends JpaRepository<entityClass, Long> {
}
