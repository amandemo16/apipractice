package com.example.practice.controller;


import com.example.practice.entity.entityClass;
import com.example.practice.service.serviceClass;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
@Api(value="oneliner", description="Operations pertaining to products in Online Store")
public class controller {

    @Autowired
    private serviceClass  serviceClass;


    @ApiOperation(value = "View a list of available employees", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("/")
    public List<entityClass> findAll(){
        return this.serviceClass.listAll();
    }



    @ApiOperation(value = "View the details of requested person", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Details not found")
    })
    @GetMapping("/{id}")

    public entityClass getDetails(@ApiParam(example = "1", required = true)@RequestParam Long id){
        return this.serviceClass.get(id);
    }

    @ApiOperation(value = "Add employee", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PostMapping("/add")
    public entityClass addDetails(@RequestBody entityClass adddetails){
        return this.serviceClass.saveDetails(adddetails);
    }


//    @PutMapping("/update")
//    public entityClass updateDetails(@RequestBody entityClass updatedetails){
//        return this.serviceClass.editDetails(updatedetails);
//    }


    @ApiOperation(value = "Update employee", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<entityClass> updateDetails(@PathVariable("id") Long id, @RequestBody entityClass update) {
        serviceClass.updateDetails(id, update);
        return new ResponseEntity<>(serviceClass.get(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Delete employee", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully Deleted"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteEmployee (@PathVariable("id") String empid) {
        try {

            this.serviceClass.deleteEmployee(Long.parseLong(empid));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            return  new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }





}
