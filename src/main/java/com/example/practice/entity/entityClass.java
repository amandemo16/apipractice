package com.example.practice.entity;


import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class entityClass {

    @Id
    @ApiModelProperty(notes = "The ID ")
    private Long Id;

    @ApiModelProperty(notes = "The name ")
    private String name;

    @ApiModelProperty(notes = "The country")
    private String country;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public entityClass(Long id, String name, String country) {
        Id = id;
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        return "entityClass{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public entityClass() {
        super();
    }




}
