package com.example.practice.service;

import com.example.practice.entity.entityClass;
import com.example.practice.repository.repositoryClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class serviceClass {

    @Autowired
    private repositoryClass repositoryClass;


    public List<entityClass> listAll(){
        return repositoryClass.findAll();
    }

    public entityClass get(Long id){
        return repositoryClass.findById(id).get();
    }

    public entityClass saveDetails(entityClass save){
        return repositoryClass.save(save);
    }

    public entityClass editDetails(entityClass edit){
        return repositoryClass.save(edit);
    }

    public void updateDetails(Long id, entityClass update) {
        entityClass updatedetails = repositoryClass.findById(id).get();
        //System.out.println(todoFromDb.toString());
        updatedetails.setId(update.getId());
        updatedetails.setName(update.getName());
        updatedetails.setCountry(update.getCountry());
        repositoryClass.save(update);
    }
    public void deleteEmployee(Long parseLong) {// delete employee by id


        repositoryClass.deleteById(parseLong);


    }


}
